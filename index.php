<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<style>
.center {
    max-width:1024px;
margin: 0 auto;
}

form {
    width: 100%;
}

form input, button {
    display:block;
margin-bottom:10px;
}

textarea, div {
   margin-bottom:10px; 
}

input[type="checkbox"] {
    display: inline-block;
}
</style>
    <title>Document</title>
</head>
<body>
<div class='center'>
<form action='/form.php' method='post'>
<input name='name' placeholder='Имя' type='text' value="<?php echo $_GET['name'] ?? '' ?>" />
<input name='phone' placeholder='Телефон'  type='text' value="<?php echo $_GET['phone'] ?? '' ?>" />
<input name='email' placeholder='Email'  type='text' value="<?php echo $_GET['email'] ?? '' ?>" />
<div>
<p>Доставка</p>
<select name="delevery">
<option>Выберите тип доставки</option>
<option value="Курьер" <?php $_GET['delevery'] === 'Курьер' ? 'selected' : ''?>>Курьер</option>
<option value="Пункт выдачи" <?php $_GET['delevery'] === 'Пункт выдачи' ? 'selected' : ''?>>Пункт выдачи</option>
</select>
</div>
<div>
<p>item1
<input type="checkbox" name="item1" <?php echo $_GET['item1'] ? 'checked': '' ?> />
<input type="number" placeholder="количество" name="item1_quantity" value="<?php echo $_GET['item1_quantity'] ?? '' ?>">
</p>
<p>item2
<input type="checkbox" name="item2" <?php echo $_GET['item2'] ? 'checked': '' ?> />
<input type="number" placeholder="количество" name="item2_quantity" value="<?php echo $_GET['item2_quantity'] ?? '' ?>">
</p>
<p>item3
<input type="checkbox" name="item3" <?php echo $_GET['item3'] ? 'checked': '' ?> />
<input type="number" placeholder="количество" name="item3_quantity" value="<?php echo $_GET['item3_quantity'] ?? '' ?>">
</p>
<p>item4
<input type="checkbox" name="item4" <?php echo $_GET['item4'] ? 'checked': '' ?> />
<input type="number" placeholder="количество" name="item4_quantity" value="<?php echo $_GET['item4_quantity'] ?? '' ?>">
</p>
<p>item5
<input type="checkbox" name="item5" <?php echo $_GET['item5'] ? 'checked': '' ?> />
<input type="number" placeholder="количество" name="item5_quantity" value="<?php echo $_GET['item5_quantity'] ?? '' ?>">
</p>
</div>
<textarea name='desc'  placeholder='Описание' ><?php echo $_GET['desc'] ?? '' ?></textarea>
<button >Отправить</button>
</form>
</div>
</body>
</html>